import {Game} from './game';
import {Config} from './config';


const game = new Game(Config);

document.getElementById('js-start-game').addEventListener('click', () => {
	game.start();
});

document.getElementById('js-reset-game').addEventListener('click', () => {
	game.reset();
});

document.getElementById(Config.containerId).addEventListener('click', (ev) => {
	if (ev.target && (<HTMLElement>ev.target).classList.contains(Config.selectedTileClass)) {
		game.addPoint();
	} else {
		game.loseLife();
	}
});