export interface GameOptions {
  gameTime: number;
  playerLives: number;
  containerId: string;
  selectedTileClass: string;
  tilesClass: string;
  tilesNumber: number;
}