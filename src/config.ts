export const Config = {
  gameTime: 60,
  playerLives: 3,
  containerId: 'grid-container',
  selectedTileClass: 'green',
  tilesClass: 'square-grid-item',
  tilesNumber: 90,
};