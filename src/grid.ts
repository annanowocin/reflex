import {GameOptions} from './game-options.interface';

export class Grid {

  constructor(private options: GameOptions) {
    this.create();
  }

  create() {
    let tiles = '';
    for (let i = 0; i < this.options.tilesNumber; i++) {
      tiles += `<div class="${this.options.tilesClass}"></div>`;
    }
    document.getElementById(this.options.containerId).innerHTML = tiles;
  }

  activateRandomTile() {
    const squares = document.getElementsByClassName(this.options.tilesClass);
    const randomTileNumber = Math.floor(Math.random() * this.options.tilesNumber * 100) % this.options.tilesNumber;
    squares[randomTileNumber].classList.add(this.options.selectedTileClass);
  }

}