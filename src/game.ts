import {Grid} from './grid';
import {GameVariables} from './game-variables';
import {GameOptions} from './game-options.interface';

export class Game {

  grid;
  gameTimeInterval: number;
  gameTilesInterval: number;
  timeLeft = 0;
  livesLeft = 0;
  score = 0;
  isPlaying = false;
  clickTimeInterval: number;

  constructor(private options: GameOptions) {
    this.grid = new Grid(options);
  }

  startTimer() {
    this.gameTimeInterval = setInterval(() => {
      this.timeLeft -= 1;
      GameVariables.setTime(this.timeLeft);
      if (this.timeLeft <= 0) {
        this.stop();
      }
    }, 1000);
  }

  activateTile() {
    this.grid.activateRandomTile();
    this.clickTimeInterval = setTimeout(() => {
      this.loseLife();
      this.grid.create();
    }, 2000);
  }

  addPoint() {
    if (!this.isPlaying) {
      return;
    }
    this.score += 1;
    GameVariables.setScore(this.score);
    this.grid.create();
    clearInterval(this.clickTimeInterval);
  }

  loseLife() {
    if (!this.isPlaying) {
      return;
    }
    this.livesLeft -= 1;
    if (this.livesLeft <= 0) {
      GameVariables.setLives(0);
      this.stop();
    } else {
      GameVariables.setLives(this.livesLeft);
      alert('straciłeś życie');
    }
  }

  start() {
    this.reset();
    this.isPlaying = true;
    this.startTimer();
    this.activateTile();
    this.gameTilesInterval = setInterval(() => {
      this.activateTile();
    }, 3000);
  }

  stop() {
    if (!this.isPlaying) {
      return;
    }
    this.isPlaying = false;
    this.stopTimers();
    alert(`koniec gry - zdobyte punkty: ${this.score}`);
  }

  stopTimers() {
    this.grid.create();
    clearInterval(this.clickTimeInterval);
    clearInterval(this.gameTilesInterval);
    clearInterval(this.gameTimeInterval);
  }

  reset() {
    this.isPlaying = false;
    this.stopTimers();
    this.score = 0;
    this.livesLeft = this.options.playerLives;
    this.timeLeft = this.options.gameTime;
    GameVariables.setScore(0);
    GameVariables.setLives(this.options.playerLives);
    GameVariables.setTime(this.options.gameTime);
  }

}