export class GameVariables {

  static setTime(secondsLeft: number) {
    document.getElementById('js-timer').innerHTML = `${secondsLeft}s`;
  }

  static setScore(score: number) {
    document.getElementById('js-points').innerHTML = `${score}`;
  }

  static setLives(lives: number) {
    document.getElementById('js-lives').innerHTML = `${lives}`;
  }

}